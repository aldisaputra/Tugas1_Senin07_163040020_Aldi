# Tugas1_Senin07_163040020_Aldi

================================

## Electronics Category :electric_plug:

- Aldi Saputra W 163040020 :tiger:
- Ayu Azzahro 163040119 :rabbit2:
- Dwi Utika Sari 163040035 :snake:
- Inggi Fara Prinanda 163040003 :horse:
- Prayoga Ildhan 163040027 :dolphin:

## Task

- [x] Create
- [x] Read
- [x] Update
- [x] Delete
- [x] Search

## Setting

Change the BASEURL to your public url, you can find BASEURL at config.php
> define('BASEURL', '`http://localhost/Tugas1/public`');'