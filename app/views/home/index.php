<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="<?= BASEURL; ?>/img/bg/1.jpg ?>" alt="First slide">
       <div class="carousel-caption">
    		<h3>Hello World</h3>
    		<p>We sell electronics product</p>
  		</div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?= BASEURL; ?>/img/bg/2.jpg ?>" alt="Second slide">
      <div class="carousel-caption">
    		<h3>Hello World</h3>
    		<p>We sell electronics product</p>
  		</div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?= BASEURL; ?>/img/bg/3.jpg ?>" alt="Third slide">
      <div class="carousel-caption">
    		<h3>Hello World</h3>
    		<p>We sell electronics product</p>
  		</div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
 <style>
  /* Make the image fully responsive */
html,body{height:98%;}
.carousel,.item,.active{height:96%;}
.carousel-inner{height:100%;}
  </style>