<style>
.logo {
    font-size: 200px;
}
@media screen and (max-width: 768px) {
    .col-sm-4 {
        text-align: center;
        margin: 25px 0;
    }
}
.jumbotron {
    background-color: #f4511e;
    color: #fff;
    padding: 100px 25px;
}

.container-fluid {
    padding: 60px 50px;
}
.bg-grey {
    background-color: #f6f6f6;
}
.logo-small {
    color: #f4511e;
    font-size: 50px;
}

.logo {
    color: #f4511e;
    font-size: 200px;
}
</style>

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About</h2>
      <h4>My team focus on</h4> 
      <h3>Electronics</h3>
    </div>
    <div class="col-sm-4">
      <span class="fa fa-signal logo"></span>
    </div>
  </div>
</div>

<div class="container-fluid bg-grey">
  <div class="row">
    <div class="col-sm-4">
      <span class="fa fa-globe logo"></span> 
    </div>
    <div class="col-sm-8">
      <h2>Our Team</h2>
      <h5><strong>Aldi Saputra Wahyudi</strong> 163040020</h5> 
      <h5><strong>Ayu Azzahro</strong> 163040119</h5>
      <h5><strong>Dwi Utika Sari</strong> 163040035</h5>
      <h5><strong>Inggi Fara Prinanda</strong> 163040003</h5>
      <h5><strong>Prayoga Ildhan</strong> 163040027</h5>
    </div>
  </div>
</div>