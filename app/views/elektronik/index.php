<?php 

if (isset($_POST['search'])) {
  $daftar = $data['result'];
}
else{
  $daftar = $data['ele'];
}
 ?>

<div class="container mt-5">
	
	   <div class="row">
			<h3>Daftar Elektronik</h3>
        <div style="float: right; padding-left: 42%;">
          <form method="post" action="<?= BASEURL; ?>/elektronik/search/">
            <div class="input-group stylish-input-group">
                <input type="text" class="form-control" id="keyword" name="keyword"  placeholder="Search" class="input-group stylish-input-group" >
                <button type="submit" class="btn-secondary" id="search" name="search"><span class="fa fa-search"></span></button> 
              
            </div>
          </form> 
        </div>    
        <button class="btn btn-lg btn-primary fa fa-plus-circle" style="margin-left: 15px;" class="btn btn-primary" data-toggle="modal" data-target="#tambahModal" id="tambah" name="tambah"> Tambah Produk</button>

      </div>
      
			<table class="table">

  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Merk</th>
      <th scope="col">Tipe</th>
      <th scope="col">Harga</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
   <?php $i = 1; ?>
   <?php foreach ($daftar as $key) : ?>
  <tbody>
    <tr>
      <th scope="row"><?= $i; ?></th>
        <td><a href="<?= BASEURL; ?>/elektronik/detail/<?= $key['id']; ?>" class="badge badge-primary"><?= $key['nama'] ?></a></td>
      <td><?= $key['merk'] ?></td>
      <td><?= $key['tipe'] ?></td>
      <?php 
        //Mengubah format string menjadi uang
        $harga=number_format($key['harga'],0,",",".");
       ?>
      <td><?= "Rp. ". $harga ?></td>
      <td>
          <a href="<?= BASEURL; ?>/elektronik/hapus/<?= $key['id']; ?>" name="hapus" class="btn btn-danger fa fa-trash" onclick="return confirm('Apakah anda yakin ?')"></a>
          <button class="btn btn-success fa fa-edit" class="btn btn-primary" data-toggle="modal" data-target="#ubahModal<?= $key['id'];?>"></button><!-- 
          <a href="<?= BASEURL; ?>/elektronik/ubah/<?= $key['id']; ?>" class="btn btn-success fa fa-edit" class="btn btn-primary"></a> -->

      </td>
    </tr>

   <?php $i++; ?>
  <?php endforeach; ?>
  </tbody>
</table>
	</div>
</div>

<?php foreach ($daftar as $key) : ?>
<div class="modal fade" id="ubahModal<?= $key['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Ubah Produk</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <form style="margin: 0px 0px; width: 100%;" method="post" action="<?= BASEURL; ?>/elektronik/ubah/">
              
              <input type="hidden" name="id" value="<?= $key['id'];?>">
                  <input type="hidden" name="gambar_lama" value="<?= $key["gambar"]?>">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama elektronik</label>
                  <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= $key["nama"];?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Merk</label>
                  <input type="text" class="form-control" name="merk" id="merk" placeholder="Merk" value="<?= $key["merk"];?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Tipe</label>
                  <input type="text" class="form-control" name="tipe" id="tipe" placeholder="Tipe" value="<?= $key["tipe"];?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Harga</label>
                  <input type="number" class="form-control" name="harga" id="harga" placeholder="Harga" value="<?= $key["harga"];?>">
                </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                     <input type="submit" name="ubah" class="btn btn-primary" 
                              role="button"
                              onclick="return confirm('Yakin akan Mengubah ?')" value="Ubah Data" /> 
                    </form>

            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>

<!-- Modal Tambah -->
<div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h5 class="modal-title" id="titleTambah">Tambah Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form style="margin: 0px 0px; width: 100%;" method="post" action="<?= BASEURL; ?>/elektronik/tambah/">
              
              <div class="form-group">
                <label for="nama">Nama Elektronik</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" >
              </div>
                <div class="form-group">
                <label for="merk">Merk</label>
                <input type="text" class="form-control" id="merk" name="merk" placeholder="Merk" >
              </div>  <div class="form-group">
                <label for="tipe">Tipe</label>
                <input type="text" class="form-control" id="tipe" name="tipe" placeholder="Tipe" >
              </div>  <div class="form-group">
                <label for="harga">Harga</label>
                <input type="number" class="form-control" id="harga" name="harga" placeholder="Harga">
              </div> 
              <div class="form-group">
                 <input type="file" name="gambar" id="gambar" name="gambar" class="form-control" placeholder="Gambar... ex: admin.jpg" />
              </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                     <input type="submit" name="tambah" class="btn btn-primary" 
                              role="button"
                              onclick="return confirm('Yakin akan Menambah ?')" value="Tambah Data" /> 
                    </form>
  </div>
</div>