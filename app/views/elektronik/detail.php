<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<div class="card-body">
			<h5 class="card-title"><img src="<?= BASEURL; ?>/img/<?= $data['ele']['gambar'] ?>"></h5>
			<h3 class="card-subtitle mb-2 text-muted"><?= $data['ele']['nama'] ?></h6>
			<p class="card-text">Merk 	:	<?= $data['ele']['merk'] ?></p>
			<p class="card-text">Tipe 	:	<?= $data['ele']['tipe'] ?></p>
			 <?php 
       			 //Mengubah format string menjadi uang
       			 $harga=number_format($data['ele']['harga'],0,",",".");
      		 ?>
			<p class="card-text">Harga 	:	<?= "Rp. ". $harga ?></p>
			<a href="<?= BASEURL ?>/elektronik" class="card-link">Kembali</a>
		</div>
	</div>
</div>