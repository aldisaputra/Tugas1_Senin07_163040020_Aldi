<?php 

/**
 * 
 */
class Elektronik extends Controller
{
	public function index()
	{
		$data = array(
			'judul' => "Daftar Elektronik",
			'ele' => $this->model('Elektronik_model')->getAllElektronik()
		);
		$this->view('templates/header', $data);
		$this->view('elektronik/index', $data);
		$this->view('templates/footer');
	}

	public function detail($id)
	{
		$data = array(
			'judul' => "Daftar Elektronik",
			'ele' => $this->model('Elektronik_model')->getElektronikById($id)
		);
		$this->view('templates/header', $data);
		$this->view('elektronik/detail', $data);
		$this->view('templates/footer');
	}

	public function hapus($id)
	{
		
		$this->model('Elektronik_model')->hapus_data($id);
		$this->view('elektronik/toIndex');
	}
	
	public function ubah(){
		$id = isset($_POST['id']) ? $_POST['id'] : NULL;
		$nama = isset($_POST['nama']) ? $_POST['nama'] : NULL;
		$merk = isset($_POST['merk']) ? $_POST['merk'] : NULL;
		$tipe =isset($_POST['tipe']) ? $_POST['tipe'] : NULL;
		$harga = isset($_POST['harga']) ? $_POST['harga'] : NULL;
		$gambar = isset($_POST['gambar_lama']) ? $_POST['gambar_lama'] : NULL;

		$data = array(
			'id' => $id,
			'nama' => $nama,
			'merk' => $merk,
			'tipe' => $tipe,
			'harga' => $harga,
			'gambar' => $gambar
			);

		$this->model('Elektronik_model')->ubah_data($data);
		$this->view('elektronik/toIndex');
	}

	public function tambah(){
		$nama = isset($_POST['nama']) ? $_POST['nama'] : NULL;
		$merk = isset($_POST['merk']) ? $_POST['merk'] : NULL;
		$tipe = isset($_POST['tipe']) ? $_POST['tipe'] : NULL;
		$harga = isset($_POST['harga']) ? $_POST['harga'] : NULL;
		$gambar = isset($_POST['gambar']) ? $_POST['gambar'] : NULL;
 
		$data = array(
			'nama' => $nama,
			'merk' => $merk,
			'tipe' => $tipe,
			'harga' => $harga,
			'gambar' => $gambar
			);
		$this->model('Elektronik_model')->tambah_data($data);
		$this->view('elektronik/toIndex');
	}
 
	 public function search()
	{
		$keyword = isset($_POST['keyword']) ? $_POST['keyword'] : NULL;
		$data = array(
			'judul' => "Daftar Elektronik",
			'result' =>  $this->model('Elektronik_model')->search_data($keyword)
			);
		$this->view('templates/header', $data);
		$this->view('elektronik/index', $data);
		$this->view('templates/footer');
	}

}