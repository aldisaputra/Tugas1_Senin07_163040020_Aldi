<?php 

/**
 * 
 */
class Elektronik_model
{

	private $table = 'elektronik';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllElektronik()
	{
		$this->db->query("SELECT * FROM " . $this->table);
		return $this->db->resultSet();
	}

	public function getElektronikById($id)
	{
		$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		return $this->db->single();
	}
	public function hapus_data($id)
	{
		$this->db->query("DELETE FROM " . $this->table . " WHERE id=:id");
		$this->db->bind('id',$id);	
		return $this->db->execute();
	}

	public function tambah_data($data){
		$this->db->query("INSERT INTO " . $this->table . " 
			(id, nama, merk, tipe, harga, gambar) VALUES 
			('', '".$data['nama']."', '".$data['merk']."', '".$data['tipe']."', '".$data['harga']."', '".$data['gambar']."')"
		);
		return $this->db->execute();
	}

	public function ubah_data($data)
	{
		$this->db->query("UPDATE " . $this->table . " SET nama = '".$data['nama']."', merk = '".$data['merk']."', tipe = '".$data['tipe']."', harga = '".$data['harga']."', gambar = '".$data['gambar']."' where id= ".$data['id']);

		return $this->db->execute();
	}
	public function search_data($keyword)
    {
        $this->db->query("SELECT * FROM " . $this->table . "
            WHERE
            nama LIKE '%$keyword%' OR 
            merk LIKE '%$keyword%' OR
            tipe LIKE '%$keyword%' OR
            harga LIKE '%$keyword%'
        	");
        $this->db->bind('keyword',$keyword);	
        return $this->db->resultSet();
    }
}